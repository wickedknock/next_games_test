﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GUI_Handler : MonoBehaviour
{
    /*  
     To be implemented
         Score
         */



    #region  GUI components
    public Button m_StartButton;
    public Button m_FireButton;
    public Button m_SpecialButton;
    public Text m_SpecialText;
    #endregion


   

    void Awake()
    {
        m_StartButton = GameObject.Find("StartButton").GetComponent<Button>();
        m_FireButton = GameObject.Find("FireButton").GetComponent<Button>();
        m_SpecialButton = GameObject.Find("SpecialButton").GetComponent<Button>();

        m_StartButton.onClick.AddListener(StartButton);

     
       
    }

    void Start()
    {
        

    }

    
    void Update()
    {
       
    }



    void OnEnable()
    {
       

    }

    void OnDisable()
    {

    }


    void StartButton()
    {
        GameStats.GS.ChangingState(GameManager.State.GameStarted);
      
        m_StartButton.gameObject.SetActive(false);
    }

    public void SpecialReadyText(GameManager.State state) 
    {
        switch (state)
        {
            case GameManager.State.PreStart:
                m_SpecialText.text = "Not Ready";
                break;
            case GameManager.State.GameStarted:
                break;
            case GameManager.State.BossFight:

                m_SpecialText.text = "Special Ready";
                break;
            case GameManager.State.GameEnd:
                break;
            default:
                break;
        }

        
    
    }


    public void AddPlayerCalls(GameObject player)
    {
       m_FireButton.onClick.AddListener(player.GetComponent<FireRocket>().Fire);
       m_SpecialButton.onClick.AddListener(player.GetComponent<FireSpecial>().Fire);
    }

}
