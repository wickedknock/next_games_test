﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlienLaser : MonoBehaviour
{
    private float speed = 50;

    private Rigidbody2D m_RigidBody;

    public Sprite m_DestroyImage;

    private GameManager GameManager;



    

    void Awake()
    {
        
        m_RigidBody = GetComponent<Rigidbody2D>();
        m_RigidBody.velocity = Vector2.down * speed * Time.deltaTime;
    }

    void Start()
    {

    }


    void Update()
    {

    }

    void OnTriggerEnter2D(Collider2D col)
    {

        if (col.gameObject.tag == "Wall")
        {
            Destroy(gameObject);
        }

        if (col.gameObject.tag == "Player")
        {

            col.gameObject.GetComponent<SpriteRenderer>().sprite = m_DestroyImage;

           
            Destroy(gameObject);

            Destroy(col.gameObject,0.5f);

            GameStats.GS.ChangingState(GameManager.State.PreStart);

        }


        if (col.gameObject.tag == "Rocket")
        {

            col.gameObject.GetComponent<SpriteRenderer>().sprite = m_DestroyImage;


            Destroy(gameObject);

            Destroy(col.gameObject, 0.5f);
        }


    }




    void OnBecameInvisible()
    {

        Destroy(gameObject);

    }
}
