﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Alien : AlienBase
{

    /*  
     To be implemented
         touch player and set state to game end
          delete them all without looping through them
         */


    //public delegate void DestroyMe();
    //public static event DestroyMe DestroyMeCaller;
    protected override void Awake()
    {
        base.Awake();

    }


    protected override void Start()
    {
        base.Start();

    }






    protected override void OnCollisionEnter2D(Collision2D col)
    {
        base.OnCollisionEnter2D(col);

    }


    protected override void ChangeDirection(int f_Direction)
    {

        base.ChangeDirection(f_Direction);

    }


    protected override void MoveDown()
    {
        base.MoveDown();
    }


    protected override void FireLasers()
    {
        base.FireLasers();

    }

   
}
