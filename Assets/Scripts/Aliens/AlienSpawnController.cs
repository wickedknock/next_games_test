﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlienSpawnController : MonoBehaviour
{
    private const float OFFSET_Y = 0.46f;
    private const float OFFSET_X = 0.54f;
    private const float START_Y = 1.43f;
    private const float START_X = -1.69f;
    private const int ROW = 4;
    private const int COLOUMN = 7;

    [SerializeField]
    private Sprite[] m_AlienSprites;
    [SerializeField]
    private GameObject m_AlienPrefab;


    void Awake()
    {

        m_AlienPrefab = Resources.Load("Prefabs/Alien") as GameObject;

    }


    void Start()
    {
      
    }

    
    void Update()
    {
        
    }



    public void SpawnAliens()
    {
        float start_x = START_X;
        float start_y = START_Y;
        

        for (int i = 0; i < ROW; i++)
        {
            
            for (int j = 0; j < COLOUMN; j++)
            {
                GameObject alien = Instantiate<GameObject>(m_AlienPrefab);
                alien.GetComponent<SpriteRenderer>().sprite = m_AlienSprites[Random.Range(0, m_AlienSprites.Length)];
                alien.transform.position = new Vector3(start_x , start_y , 0);
                start_x += OFFSET_X;
                
            }
            start_x = START_X;
            start_y -= OFFSET_Y;
        }


    }
}
