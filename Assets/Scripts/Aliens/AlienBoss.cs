﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlienBoss : AlienBase
{
    /*  
     To be implemented
         add more fire power in all directions and become invisible
         */

    public int health = 30;
   

    protected override void Awake()
    {
        base.Awake();
        speed = 100f;
    }


    protected override void Start()
    {
        base.Start();

    }






    protected override void OnCollisionEnter2D(Collision2D col)
    {
        base.OnCollisionEnter2D(col);

    }


    protected override void ChangeDirection(int f_Direction)
    {

        base.ChangeDirection(f_Direction);

    }


    protected override void MoveDown()
    {
        base.MoveDown();
    }


    protected override void FireLasers()
    {
        base.FireLasers();

    }

    private void OnEnable()
    {

    }



    private void OnDisable()
    {

    }

    private void DestroyMe()
    {
        Destroy(this.gameObject);
    }


    public void GetDamage(int dmg) 
    {

        health -= dmg;
        if (health <= 0) 
        {

            GameStats.GS.ChangingState(GameManager.State.GameEnd);
            Destroy(this.gameObject);
        
        
        }
    
    }

    
}
