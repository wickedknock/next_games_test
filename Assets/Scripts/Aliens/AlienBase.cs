﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AlienBase : MonoBehaviour
{
    protected float speed = 50f;

    protected Rigidbody2D m_RigidBody;

    protected GameObject m_AlienLaser;



    protected float m_FireRate = 0.997f;

    protected virtual void Awake()
    {
        m_RigidBody = GetComponent<Rigidbody2D>();
        m_AlienLaser = Resources.Load("Prefabs/AlienLaser") as GameObject;

    }


    protected virtual void Start()
    {
        m_RigidBody.velocity = (new Vector2(1, 0) * speed) * Time.deltaTime;

        InvokeRepeating("FireLasers", 0.1f, 0.3f);

    }






   protected virtual void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.name == "leftWall")
        {
            ChangeDirection(1);
            MoveDown();
        }
        if (col.gameObject.name == "rightWall")
        {
            ChangeDirection(-1);
            MoveDown();
        }

        if (col.gameObject.tag == "Rocket")
        {
            Destroy(col.gameObject);
            Destroy(gameObject);
        }

        if (col.gameObject.tag == "bottomWall")
        {
            GameStats.GS.ChangingState(GameManager.State.GameEnd);
        }

    }


    protected virtual void ChangeDirection(int f_Direction)
    {

        Vector2 changedVelocity = m_RigidBody.velocity;
        changedVelocity.x = (speed * f_Direction) * Time.deltaTime;
        m_RigidBody.velocity = changedVelocity;


    }


    protected virtual void MoveDown()
    {
        Vector2 newPos = transform.position;
        newPos.y -= 0.1f;
        transform.position = newPos;
    }


    protected virtual void FireLasers()
    {

        if (Random.value > m_FireRate)
        {
            Instantiate(m_AlienLaser, transform.position, Quaternion.identity);
        }

    }

}

