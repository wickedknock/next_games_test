﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStats : MonoBehaviour
{/*  
     To be implemented
         get all scripts that need to be referenced in other scripts
         */

    public static GameStats GS;
    

    public delegate void StateChanger(GameManager.State state);
    public static event StateChanger StateChangeCaller;

    public int m_NumberOfAliens = 28;
    public bool m_BossSpawned = false;

    private void Awake()
    {
        if (GS != null)
            GameObject.Destroy(GS);
        else
            GS = this;

        
    }

    void Start()
    {
       
    }

    
    void Update()
    {
        
    }

    public void ChangingState(GameManager.State state) 
    {

        StateChangeCaller(state);
    
    }

    public void CheckNumberOfAliens() 
    {
        m_NumberOfAliens = m_NumberOfAliens -1;
        if (m_NumberOfAliens == 0 && !m_BossSpawned) 
        {

            

            m_BossSpawned = true;
            ChangingState(GameManager.State.BossFight);
           
        }
    
    }

}
