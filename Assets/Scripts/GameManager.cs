﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    

    

    public enum  State { 
        PreStart,
        GameStarted,
        BossFight,
        GameEnd

    }

   
   private State m_CurrentState = State.PreStart;


    private AlienSpawnController m_AlienSpawnC;
    private GUI_Handler m_GUI_Handler;
    private GameObject m_PlayerPrefab;
    private GameObject m_Player;
    private GameObject m_BossStartPos;

    void Awake()
    {
        m_AlienSpawnC = GetComponent<AlienSpawnController>();
        m_GUI_Handler = FindObjectOfType<GUI_Handler>();
        m_PlayerPrefab = Resources.Load("Prefabs/SpaceShip_Player") as GameObject;
        m_Player = GameObject.FindGameObjectWithTag("Player");
        m_BossStartPos = GameObject.FindGameObjectWithTag("BossStart");

       
    }
    void Start()
    {
        m_GUI_Handler.AddPlayerCalls(m_Player);
    }

    
    void Update()
    {
        
    }


   public void ChangeState(State state)
    {
        m_CurrentState = state;

        switch (state)
        {
            case State.PreStart:
                {
                    SetNewGame();
                    break;
                }

            case State.GameStarted:
                {
                    StartGame();
                    break;
                }


            case State.BossFight:
                {
                    SpawnBoss();
                    break;
                }

            case State.GameEnd:
                {

                    ChangeState(State.PreStart);
                    break;
                }

            default:
                {
                   break;
                }

        }

    }

    private void SpawnBoss()
    {
        Instantiate(Resources.Load("Prefabs/Boss"), m_BossStartPos.transform.position, Quaternion.identity);
        m_GUI_Handler.SpecialReadyText(m_CurrentState);
    }

    private void StartGame()
    {
        m_AlienSpawnC.SpawnAliens();
        m_Player.GetComponent<PlayerBase>().EnableScripts();
    }

    private void SetNewGame()
    {
        GameObject[] aliens = GameObject.FindGameObjectsWithTag("Alien");
        if (aliens.Length > 0)
        {
            foreach (GameObject item in aliens)
            {
                Destroy(item.gameObject);
            }
        }

        GameObject alienBoss = GameObject.FindGameObjectWithTag("Boss");
        if (alienBoss ?? false) 
        {
            Destroy(alienBoss.gameObject);
        
        }

        if (m_Player ?? false)
        {
            Destroy(m_Player);

        }

        m_GUI_Handler.m_StartButton.gameObject.SetActive(true);
        m_Player = Instantiate(m_PlayerPrefab);
        Transform tempTransform = GameObject.FindGameObjectWithTag("PlayerSTART_XY").GetComponent<Transform>();
        m_Player.transform.position = tempTransform.position;
        m_Player.transform.localScale = tempTransform.localScale;
        m_GUI_Handler.AddPlayerCalls(m_Player);

        m_GUI_Handler.SpecialReadyText(m_CurrentState);
    }

    



    private void OnEnable()
    {
        GameStats.StateChangeCaller += ChangeState;
    }

    private void OnDisable()
    {
        GameStats.StateChangeCaller -= ChangeState;
    }

}
