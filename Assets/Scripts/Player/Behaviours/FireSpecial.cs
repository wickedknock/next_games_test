﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireSpecial : PlayerBase
{
    /*  
     To be implemented
      Regenerate with time
      easily kill boss and aliens with ray beam  
         */

    protected GameObject m_Special;
    protected GameObject m_WeaponSpawn;
    protected GameObject m_SpecialBeamObject;
    protected bool m_UseOnce = false;

    protected override void Awake()
    {

        m_WeaponSpawn = GameObject.Find("WeaponSpawn");
        m_Special = Resources.Load("Prefabs/Special") as GameObject;
    }

    void Start()
    {

    }


    void Update()
    {
        if (Input.GetButtonDown("Jump"))
        {
            if (!m_UseOnce && GameStats.GS.m_BossSpawned)
            {
                m_SpecialBeamObject = Instantiate(m_Special, transform.position, Quaternion.identity);
                m_SpecialBeamObject.transform.position = new Vector2(transform.position.x, 0);
                m_SpecialBeamObject.transform.Rotate(0, 0, 90);
                m_UseOnce = true;
            }
           

        }

        if (m_SpecialBeamObject ?? false)
        {

            m_SpecialBeamObject.gameObject.transform.position = new Vector2(transform.position.x, 0);


        }


    }

    public void Fire()
    {
        if (!m_UseOnce && GameStats.GS.m_BossSpawned)
        {
            m_SpecialBeamObject = Instantiate(m_Special, m_WeaponSpawn.transform.position, Quaternion.identity);
            m_SpecialBeamObject.transform.position = new Vector2(transform.position.x, 0);
            m_SpecialBeamObject.transform.Rotate(0, 0, 90);
            m_UseOnce = true;
        }
    }



}
