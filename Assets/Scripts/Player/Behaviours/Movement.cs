﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : PlayerBase
{

    private float speed = 80;

    public Joystick m_joystick;

    protected override void Awake()
    {
        m_RigidBody = GetComponent<Rigidbody2D>();
        m_joystick = GameObject.FindObjectOfType<FixedJoystick>();
    }

    void FixedUpdate()
    {   
        //testing with keyboard
        //float horzMove = Input.GetAxisRaw("Horizontal");
        float horzMove = m_joystick.Horizontal;
        m_RigidBody.velocity = (new Vector2(horzMove , 0)* speed) * Time.deltaTime;
        
    }

    void Start()
    {
        
    }

    
    void Update()
    {
        
    }

   


}
