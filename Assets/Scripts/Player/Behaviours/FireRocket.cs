﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireRocket : PlayerBase
{

    protected GameObject m_Rocket;
    protected GameObject m_WeaponSpawn;

    protected override void Awake()
    {
        // this was generated a null reference error on restart after death -- will check
        //m_WeaponSpawn = GameObject.Find("WeaponSpawn");
        m_Rocket = Resources.Load("Prefabs/Rocket") as GameObject;
    }

    void Start()
    {
        
    }

    
    void Update()
    {
        //for pc testing
        //if (Input.GetKeyDown(KeyCode.Space))
        //{
        //    Instantiate(m_Rocket,transform.position, Quaternion.identity);
        //} 
    }

    public void Fire()
    {
        Instantiate(m_Rocket, transform.position, Quaternion.identity);
    }

}
