﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PlayerBase : MonoBehaviour
{
   

    protected Rigidbody2D m_RigidBody;
    protected CircleCollider2D m_CircleCollider;
  
    


   protected virtual void Awake()
    {
        m_RigidBody = GetComponent<Rigidbody2D>();
        m_CircleCollider = GetComponent<CircleCollider2D>();
       


    }

    void Start()
    {
        
    }

   
    void Update()
    {
        
    }

    public void EnableScripts()
    {

        gameObject.GetComponent<Movement>().enabled = true;
        gameObject.GetComponent<FireRocket>().enabled = true;

    }


   


    
}
