﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rocket : MonoBehaviour
{

    private float speed = 200;

    private Rigidbody2D m_RigidBody;

    public Sprite m_DestroyImage;


    void Awake()
    {

        m_RigidBody = GetComponent<Rigidbody2D>();
        m_RigidBody.velocity = Vector2.up * speed * Time.deltaTime;
    }

    void Start()
    {
        
    }

   
    void Update()
    {
        
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        
        if (col.gameObject.tag =="Wall")
        {
            Destroy(gameObject);
        }

        if (col.gameObject.tag == "Alien") 
        {


            col.gameObject.GetComponent<SpriteRenderer>().sprite = m_DestroyImage;

            Destroy(gameObject);

            Destroy(col.gameObject,0.05f);

            GameStats.GS.CheckNumberOfAliens();

        }

        if (col.gameObject.tag == "AlienLaser")
        {

            col.gameObject.GetComponent<SpriteRenderer>().sprite = m_DestroyImage;

            Destroy(gameObject);

            Destroy(col.gameObject, 0.05f);

        }

        if (col.gameObject.tag == "Boss")
        {

            col.gameObject.GetComponent<AlienBoss>().GetDamage(2);

            Destroy(gameObject);

            

        }

    }

  


    void OnBecameInvisible()
    {

        Destroy(gameObject);

    }

}
