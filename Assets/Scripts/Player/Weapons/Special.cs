﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Special : MonoBehaviour
{
    public Sprite m_DestroyImage;
    void Start()
    {
       Destroy(gameObject, 3.0f);
    }

    
    void Update()
    {
        
    }


    void OnTriggerEnter2D(Collider2D col)
    {

        

        if (col.gameObject.tag == "Alien")
        {


            col.gameObject.GetComponent<SpriteRenderer>().sprite = m_DestroyImage;


            Destroy(col.gameObject, 0.05f);

            GameStats.GS.CheckNumberOfAliens();

        }

        if (col.gameObject.tag == "AlienLaser")
        {

            col.gameObject.GetComponent<SpriteRenderer>().sprite = m_DestroyImage;


            Destroy(col.gameObject, 0.05f);

        }

        if (col.gameObject.tag == "Boss")
        {

            col.gameObject.GetComponent<AlienBoss>().GetDamage(7);

            Destroy(gameObject);



        }

    }
}
